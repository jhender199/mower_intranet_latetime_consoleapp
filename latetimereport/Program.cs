﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Text;
using System.Security.Principal;
using System.Net.Mail;
using System.Configuration;

namespace latetimereport
{
    class Program
    {
        public static List<DateTime> datelist = new List<DateTime>();
        static void Main(string[] args)
        {
            TimeSpan ts = new TimeSpan(0, 0, 0);
            if(getholiday(DateTime.Now.ToString("MM/dd/yy"))== "dontrun")
            {
                return;
            }
            
            //Need a start date that is today's date and a late date that is today's date minus 4 buiness days. Exclude holidays and weekends.
            DateTime startdate = DateTime.Now.AddDays(-30);
            int adddays = -1*(3 + checkdates(DateTime.Now.AddDays(-4), DateTime.Now));
            //change to -1 from addays to get up to the previous day was -4.
            //DateTime latedate = DateTime.Now.AddDays(adddays);
           //these 3 lines handle new logic that always reports on the previous day inlcuding dealing with a monday
            DateTime latedate = DateTime.Now.AddDays(-1);
            if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
            { latedate = DateTime.Now.AddDays(-3); }
            SqlConnection conn = new SqlConnection("Data Source=EMA-SQL3;Initial Catalog=MILKY_WAY;user=satest;pwd=sadfart44");
            SqlCommand cmd = new SqlCommand("sp_GetOfficesActive", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            SqlDataReader rdr = cmd.ExecuteReader();
            string holdbody = "";

            startdate = startdate.Date + ts;
            latedate = latedate.Date + ts;
            
            
            while (rdr.Read())
            {
                string setdelinquent = "";
                Console.WriteLine("Report For " + rdr["TITLE"].ToString() + " Run " + DateTime.Now.ToString("MM/dd/yy"));
                holdbody += "<tr><td colspan=\"2\">Report For " + rdr["TITLE"].ToString() + " Run " + DateTime.Now.ToString("MM/dd/yy") + " " + DateTime.Now.ToString("hh:mm:ss") + "</td></tr>";

                for (DateTime date = startdate; date <= latedate; date = date.AddDays(1))
                {
                    if (datelist.Exists(item=> item==date) || date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
                    {
                        //Skip becuase it's a holiday or weekend
                    }
                    else
                    {
                        SqlConnection conn2 = new SqlConnection("Data Source=EMA-SQL3;Initial Catalog=MILKY_WAY;user=satest;pwd=sadfart44");
                        SqlCommand cmd2 = new SqlCommand("sp_GetLateTimesheets2", conn2);
                        cmd2.Parameters.Add("@date", date.ToString("MM/dd/yy"));
                        cmd2.Parameters.Add("@officeid", rdr["ID"].ToString());
                        cmd2.CommandType = CommandType.StoredProcedure;
                        conn2.Open();
                        SqlDataReader rdr2 = cmd2.ExecuteReader();
                        int counter=0;
                        //need to write code to take into account all of the overrides
                        
                        while (rdr2.Read())
                        {
                            string oddhours = "";
                            string skipit="";
                            //Add to skip dates if before hire date
                            try { if (Convert.ToDateTime(rdr2["HRIS_ANNIVDATE"]) > date) { skipit = "true"; } } catch { }
                            
                            SqlConnection conn3 = new SqlConnection("Data Source=EMA-SQL3;Initial Catalog=MILKY_WAY;user=satest;pwd=sadfart44");
                            SqlCommand cmd3 = new SqlCommand("sp_GetCupssEmp", conn3);
                            cmd3.Parameters.Add("@empcode", rdr2["EMP_CODE"].ToString());
                            cmd3.CommandType = CommandType.StoredProcedure;
                            conn3.Open();
                            SqlDataReader rdr3 = cmd3.ExecuteReader();
                       
                            while (rdr3.Read())
                            {
                                if(rdr3["MON_HRS"].ToString() =="" && date.DayOfWeek==DayOfWeek.Monday){skipit="true";}
                                if (rdr3["TUE_HRS"].ToString() == "" && date.DayOfWeek == DayOfWeek.Tuesday) { skipit = "true"; }
                                if (rdr3["WED_HRS"].ToString() == "" && date.DayOfWeek == DayOfWeek.Wednesday) { skipit = "true"; }
                                if (rdr3["THU_HRS"].ToString() == "" && date.DayOfWeek == DayOfWeek.Thursday) { skipit = "true"; }
                                if (rdr3["FRI_HRS"].ToString() == "" && date.DayOfWeek == DayOfWeek.Friday) { skipit = "true"; }
                            }
                            conn3.Close();
                            string theoverride = rdr2["OVERRIDE"].ToString();
                            string[] overrideit = rdr2["OVERRIDE"].ToString().Split(',');
                            string[] overrideitdash = rdr2["OVERRIDE"].ToString().Split('-');
                            if (overrideit.Length > 1 || overrideitdash.Length > 1)
                            {
                                foreach(string oneoverride in overrideit)
                                {
                                    if(overrideitdash.Length == 2)
                                    {
                                        if (Convert.ToDateTime(overrideitdash[0]) < date && Convert.ToDateTime(overrideitdash[1]) > date)
                                        { skipit = "true"; }
                                    }
                                    else
                                    {
                                        int n;
                                        bool isNumeric = int.TryParse(oneoverride, out n);
                                        if (!isNumeric)
                                        {
                                            if (Convert.ToDateTime(oneoverride).ToString("MM/dd/yy") == date.ToString("MM/dd/yy"))
                                            {
                                                skipit = "true";
                                            }
                                        }
                                        else
                                        {
                                            if (Convert.ToInt16(oneoverride) == Convert.ToInt16(date.DayOfWeek) + 1 || theoverride == "*")
                                            {
                                                skipit = "true";
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (theoverride != "" )
                                {
                                    if (theoverride == "*") { skipit = "true"; }
                                    if (date.ToString("MM/dd/yy") == theoverride) { skipit = "true"; }
                                    int n;
                                    bool isNumeric = int.TryParse(theoverride, out n);

                                    if (theoverride != "" && skipit != "true" && isNumeric)
                                    {
                                        if ((Convert.ToInt16(theoverride) >= 1 && Convert.ToInt16(theoverride) <= 7) && Convert.ToInt16(theoverride) == Convert.ToInt16(date.DayOfWeek) + 1)
                                        { skipit = "true"; }
                                    }
                                }
                            }
                            //byte[] ASCIIValues = Encoding.ASCII.GetBytes(theoverride);

                            /* if (skipit == "true") { Console.WriteLine(rdr2["EMP_CODE"].ToString() + " - " + date + " - " + theoverride);
                             if (rdr2["EMP_CODE"].ToString() == "1530")
                             {
                                 foreach (byte b in ASCIIValues)
                                 {
                                     Console.WriteLine(b);
                                 }
                             }
                            
                             }*/
                            if (rdr2["EMP_TOT_HRS"].ToString() == "" && Convert.ToDecimal(rdr2["HRIS_HOURS_WEEK"].ToString()) < 33) oddhours= "(**Part Timer) ";
                            if (rdr2["EMP_DATE"].ToString() != "" && Convert.ToDecimal(rdr2["EMP_TOT_HRS"].ToString())>= 0 && Convert.ToDecimal(rdr2["EMP_TOT_HRS"].ToString()) < 7 && skipit=="")
                            {
                                if(Convert.ToDecimal(rdr2["HRIS_HOURS_WEEK"].ToString()) >32)
                                {
                                    if (Convert.ToDecimal(rdr2["EMP_TOT_HRS"].ToString()) > 0 && Convert.ToDecimal(rdr2["EMP_TOT_HRS"].ToString()) < 4)
                                    {
                                        oddhours = "( **Low Hours " + rdr2["EMP_TOT_HRS"].ToString() + " hours )";
                                    }
                                }
                                else
                                {
                                     oddhours = "( ** Part Time " + rdr2["EMP_TOT_HRS"].ToString() + " hours )";
                                }
                            }
                            
                            if ((rdr2["EMP_TOT_HRS"].ToString() == "" || Convert.ToDecimal(rdr2["EMP_TOT_HRS"].ToString()) < 4) && skipit == "")
                            {
                                
                                if (counter == 0)
                                {
                                    //logic can be used to deliver a message about where the cutoff for good vs bad. Currently commneted out.
                                    if (date >= latedate.AddDays(-2) && setdelinquent=="")
                                    {
                                       // holdbody += "<tr><td colspan=\"2\">*****************ABOVE THIS LINE IS CONSIDERED DELIQUENT************************</td></tr>";
                                        setdelinquent = "yes";
                                    }
                                    holdbody += "<tr><td colspan=\"2\"><BR><BR>" + date.ToString("MM/dd/yy") + "<BR><BR></td></tr>";
                                    counter = counter + 1;
                                }
                                holdbody += "<tr><td>" + rdr2["EMP_CODE"].ToString() + "-" + rdr2["FAMILIAR_FIRST_NAME"].ToString() + " " + rdr2["FAMILIAR_LAST_NAME"].ToString() + oddhours + "</td><td>(Supervisor: " + rdr2["HRIS_SUPERVISOR"].ToString() + ")</td></tr>";
                            }
                        }
                        conn2.Close();
                    }
                }
                
                doemail(holdbody, rdr["TITLE"].ToString());
                holdbody = "";
            }
            conn.Close();
            
            movejobs();
            
        }
        public static string getholiday(string thedate)
        {
            SqlConnection conn = new SqlConnection("Data Source=EMA-SQL3;Initial Catalog=MILKY_WAY;user=satest;pwd=sadfart44");
            SqlCommand cmd = new SqlCommand("sp_GetHolidays", conn);
            //cmd.Parameters.Add("@empnum", empcode);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            SqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                datelist.Add(Convert.ToDateTime( rdr["dates"].ToString() ));
                if (thedate == rdr["dates"].ToString() || Convert.ToDateTime(thedate).DayOfWeek == DayOfWeek.Saturday || Convert.ToDateTime(thedate).DayOfWeek == DayOfWeek.Sunday)
                {
                    return "dontrun";
                }

            }
            return "run";
        }
        public static int checkdates(DateTime stdate,DateTime endate)
        {
            int adddays = 0;
            for (DateTime date = stdate; date <= endate; date = date.AddDays(1))
            {
                if (date.DayOfWeek==DayOfWeek.Saturday ||date.DayOfWeek==DayOfWeek.Sunday )
                { adddays = adddays + 1; }
            }
            SqlConnection conn = new SqlConnection("Data Source=EMA-SQL3;Initial Catalog=MILKY_WAY;user=satest;pwd=sadfart44");
            SqlCommand cmd = new SqlCommand("sp_GetHolidays", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            SqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                if (Convert.ToDateTime(rdr["dates"].ToString()) >=stdate.AddDays(-1*adddays) && Convert.ToDateTime(rdr["dates"].ToString())  <= endate)
                { adddays = adddays + 1; }
            }
            int test = adddays;
            return adddays;
        }
        public static void doemail(string thebody, string thesubject)
        {
            
            MailMessage mail = new MailMessage();
            mail.From = new System.Net.Mail.MailAddress("netwit@mower.com");
            mail.To.Add(new MailAddress(ConfigurationManager.AppSettings["Webmaster"]));
            mail.To.Add(new MailAddress(ConfigurationManager.AppSettings["Accounting"]));
            mail.To.Add(new MailAddress(ConfigurationManager.AppSettings["CAA"]));
            if (thesubject == "Syracuse") mail.To.Add(new MailAddress(ConfigurationManager.AppSettings["Syracuse"]));
            if (thesubject == "Rochester") mail.To.Add(new MailAddress(ConfigurationManager.AppSettings["Rochester"]));
            if (thesubject == "New York") mail.To.Add(new MailAddress(ConfigurationManager.AppSettings["New York City"]));
            if (thesubject == "Cincinnati") mail.To.Add(new MailAddress(ConfigurationManager.AppSettings["Cincinnati"]));
            if (thesubject == "Charlotte") mail.To.Add(new MailAddress(ConfigurationManager.AppSettings["Charlotte"]));
            if (thesubject == "Buffalo") mail.To.Add(new MailAddress(ConfigurationManager.AppSettings["Buffalo"]));
            if (thesubject == "Boston") mail.To.Add(new MailAddress(ConfigurationManager.AppSettings["Boston"]));
            if (thesubject == "Atlanta") mail.To.Add(new MailAddress(ConfigurationManager.AppSettings["Atlanta"]));
            if (thesubject == "Albany") mail.To.Add(new MailAddress(ConfigurationManager.AppSettings["Albany"]));
            mail.IsBodyHtml = true;
            string holdheader = "<table width=\"70%\" style=\"font-family:verdana,arial; font-size:12px;\"><tr><td colspan=\"2\">****Report Run " + DateTime.Now.ToString("MM/dd/yy") + " ****</td></tr><tr><td colspan=\"2\"><strong>****NOW REPORTING ONE DAY LATE****</strong></td></tr><tr><td colspan=\"2\">This is an automatic report generated by NetWit.</td></tr>";
            string holdfooter = "</table>";
            mail.Body = holdheader + thebody + holdfooter;
            mail.Subject = "Late Timesheets for office " + thesubject;
            mail.Sender = new System.Net.Mail.MailAddress("netwit@mower.com");
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "192.168.1.154";
            smtp.Send(mail);

        }
        protected static void movejobs()
        {
            string strBody = "The following jobs have been moved from Advantage to Netwit.";
            SqlConnection conn = new SqlConnection("Data Source=EMA-SQL3;Initial Catalog=MILKY_WAY;user=satest;pwd=sadfart44");
            conn.Open();
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandText = "sp_Nightly_ADVtoNetwit_OpenJobs";
            sqlcmd.CommandType = System.Data.CommandType.StoredProcedure;
            sqlcmd.Connection = conn;
            SqlDataReader reader = sqlcmd.ExecuteReader();
            while (reader.Read())
            {
                strBody += "\n\n";
                strBody += "ID: " + reader["ID"].ToString() + "\n";
                strBody += "JOB NUMBER: " + reader["Job_Number"].ToString() + "\n";
                strBody += "CL_CODE: " + reader["CL_CODE"].ToString() + "\n";
                strBody += "DIV_CODE: " + reader["DIV_CODE"].ToString() + "\n";
                strBody += "PRD_CODE: " + reader["PRD_CODE"].ToString() + "\n";
                strBody += "OPENDATE_NW: " + reader["OPENDATE_NW"].ToString() + "\n";
                strBody += "OPENDATE_AD: " + reader["OPENDATE_AD"].ToString() + "\n";
                strBody += "TITLE: " + reader["TITLE"].ToString() + "\n";
                strBody += "DESCRIPTION: " + reader["DESCRIPTION"].ToString() + "\n";
                strBody += "DIV_CODE: " + reader["DIV_CODE"].ToString() + "\n\n";
            }
            conn.Close();
            reader.Close();
            MailMessage mail = new MailMessage();
            mail.From = new System.Net.Mail.MailAddress("netwit@mower.com");
            mail.To.Add(new MailAddress(ConfigurationManager.AppSettings["Webmaster"]));
            //mail.IsBodyHtml = true;
            mail.Body = strBody;
            mail.Subject = "Opened jobs have been moved from Advantage to Netwit.";
            mail.Sender = new System.Net.Mail.MailAddress("netwit@mower.com");
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "192.168.1.154";
            smtp.Send(mail);
        }
    }
}
